const { GraphQLObjectType } = require('graphql');
const etsyAPIFields = require('./fields/etsy');

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    ...etsyAPIFields,
  },
});

module.exports = RootQuery;
