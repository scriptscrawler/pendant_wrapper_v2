const axios = require('axios');
const { GraphQLString } = require('graphql');

const { CartType } = require('../types/etsy');
const { ETSY_ACCESS_KEY: eak } = process.env;
const BASE_URL = 'https://openapi.etsy.com/v2/';
const GUESTS = 'guests/';

const etsyMutations = {
  add_to_cart: {
    type: CartType,
    args: {
      listing_id: { type: GraphQLString },
      guest_id: { type: GraphQLString },
    },
    resolve(parentValue, { listing_id, guest_id }) {
      return axios
        .post(
          `${BASE_URL}${GUESTS}${guest_id}/carts?&quantity=1&api_key=${eak}`,
          { listing_id, guest_id }
        )
        .then(({ data: { results } }) => {
          return results.filter(cart => cart.shop_name === 'Pendantwrapper')[0];
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
  remove_from_cart: {
    type: CartType,
    args: {
      listing_id: { type: GraphQLString },
      guest_id: { type: GraphQLString },
    },
    resolve(parentValue, { listing_id, guest_id }) {
      return axios
        .delete(
          `${BASE_URL}${GUESTS}${guest_id}/carts?&listing_id=${listing_id}&quantity=1&api_key=${eak}`,
          { listing_id, guest_id }
        )
        .then(({ data: { results } }) => {
          return results.filter(cart => cart.shop_name === 'Pendantwrapper')[0];
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
  edit_cart_listing_quantity: {
    type: CartType,
    args: {
      listing_id: { type: GraphQLString },
      guest_id: { type: GraphQLString },
      quantity: { type: GraphQLString },
    },
    resolve(parentValue, { listing_id, guest_id, quantity }) {
      return axios
        .put(`${BASE_URL}${GUESTS}${guest_id}/carts?api_key=${eak}`, {
          listing_id,
          quantity,
          guest_id,
        })
        .then(({ data: { results } }) => {
          return results.filter(cart => cart.shop_name === 'Pendantwrapper')[0];
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
};

module.exports = etsyMutations;
