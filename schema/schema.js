const { GraphQLSchema } = require('graphql');
const query = require('./RootQuery.js');
const mutation = require('./Mutations.js');

module.exports = new GraphQLSchema({
  query,
  mutation,
});
