const graphql = require('graphql');

const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList } = graphql;

const ListingType = new GraphQLObjectType({
  name: 'Listing',
  fields: {
    listing_id: { type: GraphQLInt },
    state: { type: GraphQLString },
    user_id: { type: GraphQLInt },
    category_id: { type: GraphQLInt },
    title: { type: GraphQLString },
    description: { type: GraphQLString },
    price: { type: GraphQLString },
    currency_code: { type: GraphQLString },
    quantity: { type: GraphQLInt },
    tags: { type: GraphQLList(GraphQLString) },
    category_path: { type: GraphQLList(GraphQLString) },
    materials: { type: GraphQLList(GraphQLString) },
    shop_section_id: { type: GraphQLInt },
    url: { type: GraphQLString },
    views: { type: GraphQLInt },
    shipping_template_id: { type: GraphQLString },
    processing_min: { type: GraphQLInt },
    processing_max: { type: GraphQLInt },
  },
});

const CartType = new GraphQLObjectType({
  name: 'Cart',
  fields: {
    cart_id: { type: GraphQLString },
    shop_name: { type: GraphQLString },
    message_to_seller: { type: GraphQLString },
    destination_country_id: { type: GraphQLInt },
    coupon_code: { type: GraphQLString },
    currency_code: { type: GraphQLString },
    total: { type: GraphQLString },
    subtotal: { type: GraphQLString },
    shipping_cost: { type: GraphQLInt },
    tax_cost: { type: GraphQLString },
    discount_amount: { type: GraphQLString },
    shipping_discount_amount: { type: GraphQLInt },
    tax_discount_amount: { type: GraphQLString },
    url: { type: GraphQLString },
    listings: { type: GraphQLList(ListingType) },
  },
});

const ShopInfoType = new GraphQLObjectType({
  name: 'ShopInfo',
  fields: {
    shop_id: { type: GraphQLInt },
    shop_name: { type: GraphQLString },
    policy_welcome: { type: GraphQLString },
    policy_payment: { type: GraphQLString },
    policy_shipping: { type: GraphQLString },
    policy_refunds: { type: GraphQLString },
    announcement: { type: GraphQLString },
  },
});

const GuestType = new GraphQLObjectType({
  name: 'Guest',
  fields: {
    guest_id: { type: GraphQLString },
    checkout_url: { type: GraphQLString },
  },
});

const ListingImageType = new GraphQLObjectType({
  name: 'ListingImage',
  fields: {
    listing_image_id: { type: GraphQLInt },
    listing_id: { type: GraphQLInt },
    rank: { type: GraphQLInt },
    url_75x75: { type: GraphQLString },
    url_170x135: { type: GraphQLString },
    url_570xN: { type: GraphQLString },
    url_fullxfull: { type: GraphQLString },
    full_height: { type: GraphQLInt },
    full_width: { type: GraphQLInt },
  },
});

module.exports = {
  ListingImageType,
  GuestType,
  ShopInfoType,
  CartType,
  ListingType,
};
