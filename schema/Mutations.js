const { GraphQLObjectType } = require('graphql');
const etsyMutations = require('./mutations/etsy');

const Mutations = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    ...etsyMutations,
  },
});

module.exports = Mutations;
