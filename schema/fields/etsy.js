const axios = require('axios');
const { GraphQLString, GraphQLList, GraphQLInt } = require('graphql');
const {
  ListingImageType,
  GuestType,
  ShopInfoType,
  CartType,
  ListingType,
} = require('../types/etsy');
const { ETSY_ACCESS_KEY: eak } = process.env;
const BASE_URL = 'https://openapi.etsy.com/v2/';
const GUESTS = 'guests/';
const SHOPS = 'shops/';
const LISTINGS = 'listings/';

const etsyAPIFields = {
  shopinfo: {
    type: ShopInfoType,
    resolve(parentValue, args) {
      return axios
        .get(`${BASE_URL}${SHOPS}10382897?api_key=${eak}`)
        .then(({ data: { results } }) => {
          const [shopInfo] = results;
          return shopInfo;
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
  shoplistings: {
    type: GraphQLList(ListingType),
    resolve(parentValue, args) {
      return axios
        .get(
          `${BASE_URL}${SHOPS}10382897/${LISTINGS}active?api_key=${eak}&sort_on=created`
        )
        .then(({ data: { results } }) => {
          console.log(results);
          return results;
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
  guest: {
    type: GuestType,
    resolve(parentValue, args) {
      return axios
        .get(`${BASE_URL}${GUESTS}generator?api_key=${eak}`)
        .then(({ data: { results } }) => {
          return results[0];
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
  listingimages: {
    type: GraphQLList(ListingImageType),
    args: {
      listing_id: { type: GraphQLString },
    },
    resolve(parentValue, { listing_id }) {
      return axios
        .get(
          `${BASE_URL}${LISTINGS}/${listing_id}/images?api_key=${eak}&sort_on=created`
        )
        .then(({ data: { results } }) => {
          return results;
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
  cart: {
    type: CartType,
    args: {
      guest_id: { type: GraphQLString },
    },
    resolve(parentValue, { guest_id }) {
      return axios
        .get(`${BASE_URL}${GUESTS}${guest_id}/carts?&api_key=${eak}`)
        .then(({ data: { results } }) => {
          return results.filter(cart => cart.shop_name === 'Pendantwrapper')[0];
        })
        .catch(err => {
          return console.log(err);
        });
    },
  },
};

module.exports = etsyAPIFields;
