require('dotenv').config();
const express = require('express');
const cors = require('cors');
const expressGraphQL = require('express-graphql');
const path = require('path');
const schema = require('./schema/schema');
const mongoose = require('mongoose');

const app = express();
const { PORT, MONGO_DB_URI } = process.env;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use(express.static(path.join(__dirname, 'public', 'dist')));
app.use(express.static(path.join(__dirname, 'public', 'assets')));
app.use(
  '/graphql',
  expressGraphQL({
    schema,
    graphiql: true,
  })
);

require('./routing')(app);

mongoose.connect(
  MONGO_DB_URI,
  err => {
    if (err) {
      //Log error here with Custom Logger
      return console.log(err);
    }

    app.listen(PORT, () => {
      console.log(
        `App listening on PORT${PORT}\nMongo successfully connected!`
      );
    });
  }
);
