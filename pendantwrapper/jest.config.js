module.exports = {
  testPathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/pendantwrapper/node_modules/',
    '<rootDir>/dist/',
  ],
  collectCoverageFrom: ['**/pendantwrapper/**/*.js', '!**/app/**/tests/**'],
  coverageThreshold: {
    global: {
      statements: 85,
      branches: 85,
      lines: 85,
      functions: 85,
    },
  },
  testEnvironment: 'jsdom',
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  coverageReporters: ['json', 'lcov', 'clover'],
  resetMocks: true,
  reporters: ['default'],
  notify: true,
  watchPlugins: [
    ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
  ],
  snapshotSerializers: ['enzyme-to-json/serializer'],
};
