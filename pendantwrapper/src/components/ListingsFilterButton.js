import React from 'react';
import styled, { keyframes, css } from 'styled-components';
import {
  PRIMARY_PINK_RGB,
  PRIMARY_PINK,
  SUCESS_GREEN_RGB,
} from '../CSS_SHARED';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
const hoverFilterAnimation = keyframes`
    0%{
    background: rgba(${PRIMARY_PINK_RGB}, 0.2);

    }
    100%{
        background: rgba(${PRIMARY_PINK_RGB}, 0.9);

    }
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  font-size: 14px;
  border-radius: 100px;
  padding: 8px 25px;
  margin-top: 8px;
  margin-bottom: 8px;
  white-space: nowrap;
  border: 1px solid ${({ selected }) => (selected ? 'black' : 'transparent')};
  background: ${({ selected }) => (selected ? 'black' : 'white')};
  color: ${({ selected }) => (selected ? PRIMARY_PINK : 'inherit')};

  ${({ selected }) =>
    !selected &&
    css`
      &:hover {
        animation: ${hoverFilterAnimation} 1s forwards;
        border: 1px solid rgba(0, 0, 0, 0.3);
      }
    `}

  > svg {
    color: rgb(${PRIMARY_PINK_RGB});
    margin-left: 15px;
  }
`;

const ListingsFilterButton = props => {
  const {
    handleSelectedFilterNameState,
    selected,
    buttonText,
    uniqueId,
    parentId,
  } = props;
  return (
    <div>
      <ButtonWrapper
        onClick={() => handleSelectedFilterNameState(parentId, uniqueId)}
        selected={selected}
      >
        {buttonText}
        {selected && <FontAwesomeIcon icon={faCheckCircle} />}
      </ButtonWrapper>
    </div>
  );
};

export default ListingsFilterButton;
