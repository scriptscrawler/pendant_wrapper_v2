import React from 'react';
import { withRouter } from 'react-router';

import { NavBarWrapper, NavTextWrapper } from './NavBarComponents';
import NavButton from './NavButton';
import NavBanner from './NavBanner';
import { PATH_MAP, NAV_BUTTON_CONFIG } from '../../constants';
import CartNavButton from './CartNavButton';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import bannerImage from '../../images/single-clean.png';

const navBarWrapperProps = {
  bcolor: 'black',
};

const NavBar = props => {
  const {
    location: { pathname },
    guest_id,
  } = props;

  return (
    <NavBarWrapper {...navBarWrapperProps}>
      <NavBanner />
      <NavTextWrapper>
        <img src={bannerImage} alt="logo" />
        {NAV_BUTTON_CONFIG.map(({ text, icon, path, disabled }, i) => {
          const buttonProps = {
            guest_id,
            key: i,
            selected: PATH_MAP[pathname] === i,
            text,
            path,
            icon,
            disabled,
          };

          if (icon === faCartPlus) {
            return <CartNavButton {...buttonProps} />;
          }

          return <NavButton {...buttonProps} />;
        })}
      </NavTextWrapper>
    </NavBarWrapper>
  );
};

export default withRouter(NavBar);
