import { enzymeRenderCreator } from '../../../setupTests';
import NavBanner from '../NavBanner';

describe('NavBanner unit tests ', () => {
  const NavBannerRenderMethods = new enzymeRenderCreator(NavBanner);

  let NavBannerComp;
  beforeEach(() => {
    NavBannerComp = NavBannerRenderMethods.shallowRender();
  });
  test('should render two images', () => {
    const images = NavBannerComp.find('img');

    expect(images.length).toEqual(2);
  });

  test('should render two images with correct src', () => {
    const images = NavBannerComp.find('img');

    images.map(img => {
      const src = img.prop('src');

      expect(src).toEqual('single-clean.png');
    });
  });

  test('should render correct text', () => {
    const bannerText = NavBannerComp.find('span').prop('children');

    expect(bannerText).toEqual(`Candy's Pendant Wrapping`);
  });
});
