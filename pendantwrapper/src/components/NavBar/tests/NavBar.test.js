import NavBar from '../NavBar';
import { enzymeRenderCreator } from '../../../setupTests';

describe('NavBar unit tests', () => {
  const defaultProps = {};
  const NavBarMethodRenderer = new enzymeRenderCreator(NavBar, defaultProps);
  test('should render', () => {
    const navBarComp = NavBarMethodRenderer.shallowRender();

    expect(navBarComp.exists()).toEqual(true);
  });
});
