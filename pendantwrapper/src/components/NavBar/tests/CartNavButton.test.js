import React from 'react';
import { shallow } from 'enzyme';

import { CartNavButton } from '../CartNavButton';
import { enzymeRenderCreator } from '../../../setupTests';
import NavButton from '../NavButton';

describe('CartNavButton Unit Tests', () => {
  const CartNavButtonRenderer = new enzymeRenderCreator(CartNavButton, {
    loading: true,
  });

  test('should render the NavButton component if loading is false', () => {
    const CartNavButtonComp = CartNavButtonRenderer.shallowRender({
      loading: false,
    });
    console.log(CartNavButtonComp.debug());
    const NavButtonComp = CartNavButtonComp.find(NavButton);

    expect(NavButtonComp.exists()).toEqual(true);
  });
  test.skip('should provide the correct props to NavButton', () => {});
  test('should not render the NavButton component if loading is true', () => {
    const CartNavButtonComp = CartNavButtonRenderer.shallowRender();

    const NavButtonComp = CartNavButtonComp.find(NavButton);

    expect(NavButtonComp.exists()).toEqual(false);
  });
});
