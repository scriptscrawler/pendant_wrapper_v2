import { Link } from 'react-router-dom';

import { enzymeRenderCreator } from '../../../setupTests';
import { NavButton } from '../NavButton';
import { ButtonWrapper, IconWrapper, Line } from '../NavBarComponents';
import { faCalculator } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

describe('NavButton Unit Tests ', () => {
  const defaultProps = {
    icon: null,
    disabled: false,
    text: 'Some text',
    path: '/something',
    selected: false,
  };
  const NavButtonRenderer = new enzymeRenderCreator(NavButton, defaultProps);
  describe('When icon props is falsy and path prop truthy', () => {
    let NavBarComp;

    beforeEach(() => {
      NavBarComp = NavButtonRenderer.shallowRender();
    });

    test('should render a Button wrapper', () => {
      const ButtonWrapperComp = NavBarComp.find(ButtonWrapper);

      expect(ButtonWrapperComp.length).toEqual(1);
      expect(ButtonWrapperComp.exists()).toEqual(true);
    });

    test('should render a Link tag wrapping the text if path is truthy', () => {
      const LinkComp = NavBarComp.find(Link);

      expect(LinkComp.length).toEqual(1);
      expect(LinkComp.exists()).toEqual(true);
    });

    test('should supply correct path to the Link tag when path is truthy', () => {
      const LinkCompToProp = NavBarComp.find(Link).prop('to');

      expect(LinkCompToProp).toEqual('/something');
    });

    test('should render correct supplied text', () => {
      const LinkCompChildrenProp = NavBarComp.find(Link).prop('children');

      expect(LinkCompChildrenProp).toEqual('Some text');
    });

    test('should supply correct props to ButtonWrapper', () => {
      const ButtonWrapperCompProps = NavBarComp.find(ButtonWrapper).props();

      expect(ButtonWrapperCompProps).toMatchSnapshot();
    });
  });

  describe('When icon props is truthy', () => {
    let NavBarComp;

    beforeEach(() => {
      NavBarComp = NavButtonRenderer.shallowRender({ icon: faCalculator });
    });
    test('should render an Icon wrapper', () => {
      const IconWrapperComp = NavBarComp.find(IconWrapper);

      expect(IconWrapperComp.exists()).toEqual(true);
    });

    test('should render a Link tag wrapping the icon', () => {
      const LinkWrapperComp = NavBarComp.find(Link);

      expect(LinkWrapperComp.exists()).toEqual(true);
    });

    test('should supply correct icon to the FontAwesome Icon Component', () => {
      const FontAwesomeIconProp = NavBarComp.find(FontAwesomeIcon).prop('icon');

      expect(FontAwesomeIconProp).toEqual(faCalculator);
    });

    test('should supply correct path to the Link tag', () => {
      const LinkCompToProp = NavBarComp.find(Link).prop('to');

      expect(LinkCompToProp).toEqual('/something');
    });

    test('should supply correct props to IconWrapper', () => {
      const IconWrapperCompProps = NavBarComp.find(IconWrapper).props();

      expect(IconWrapperCompProps).toMatchSnapshot();
    });
  });
  describe('when we have a disabled link', () => {
    let NavBarComp;

    beforeEach(() => {
      NavBarComp = NavButtonRenderer.shallowRender({ disabled: true });
    });

    test('should render a line next to a disabled link', () => {
      const LineComp = NavBarComp.find(Line);

      expect(LineComp.length).toEqual(1);
      expect(LineComp.exists()).toEqual(true);
    });

    test('should render correct text', () => {
      const LinkCompText = NavBarComp.find(Link).prop('children');

      expect(LinkCompText).toEqual('Some text');
    });
  });
});
