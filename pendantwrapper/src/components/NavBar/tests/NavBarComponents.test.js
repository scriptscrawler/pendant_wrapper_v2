import { enzymeRenderCreator } from '../../../setupTests';

import {
  NavBarWrapper,
  NavTextWrapper,
  NavBannerWrapper,
  ButtonWrapper,
  Line,
  IconWrapper,
} from '../NavBarComponents';

describe('NavBar Components Unit Tests ', () => {
  describe('NavBannerWrapper ', () => {
    test('should render with correct css', () => {
      const NavBannerWrapperComp = new enzymeRenderCreator(
        NavBannerWrapper
      ).shallowRender();

      expect(NavBannerWrapperComp).toMatchSnapshot();
    });
  });

  describe('NavTextWrapper ', () => {
    test('should render with correct css', () => {
      const NavTextWrapperComp = new enzymeRenderCreator(
        NavTextWrapper
      ).shallowRender();

      expect(NavTextWrapperComp).toMatchSnapshot();
    });
  });

  describe('NavBarWrapper ', () => {
    test('should supply the correct props as css', () => {
      const NavBarWrapperComp = new enzymeRenderCreator(NavBarWrapper, {
        bcolor: 'purple',
        testProp: 'green',
      }).shallowRender();

      expect(NavBarWrapperComp).toMatchSnapshot();
    });
  });

  describe('ButtonWrapper ', () => {
    test('should render with correct css when button is selected and not disabled', () => {
      const ButtonWrapperComp = new enzymeRenderCreator(NavBarWrapper, {
        selected: true,
        disabled: false,
      }).shallowRender();

      expect(ButtonWrapperComp).toMatchSnapshot();
    });

    test('should render with correct css when button is not selected and not disabled', () => {
      const ButtonWrapperComp = new enzymeRenderCreator(NavBarWrapper, {
        selected: false,
        disabled: false,
      }).shallowRender();

      expect(ButtonWrapperComp).toMatchSnapshot();
    });

    test('should render with correct css when button is not selected and is disabled', () => {
      const ButtonWrapperComp = new enzymeRenderCreator(NavBarWrapper, {
        selected: false,
        disabled: true,
      }).shallowRender();

      expect(ButtonWrapperComp).toMatchSnapshot();
    });
  });

  describe('IconWrapper ', () => {
    test('should supply the correct props as css when selected is false', () => {
      const IconWrapperComp = new enzymeRenderCreator(IconWrapper, {
        selected: false,
      }).shallowRender();

      expect(IconWrapperComp).toMatchSnapshot();
    });

    test('should supply the correct props as css when selected is true', () => {
      const IconWrapperComp = new enzymeRenderCreator(IconWrapper, {
        selected: true,
      }).shallowRender();

      expect(IconWrapperComp).toMatchSnapshot();
    });
  });

  describe('Line ', () => {
    test('should supply the correct props as css when selected is false', () => {
      const LineComp = new enzymeRenderCreator(Line).shallowRender();

      expect(LineComp).toMatchSnapshot();
    });
  });
});
