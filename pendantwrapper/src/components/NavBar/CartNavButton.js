import React from 'react';
import { graphql } from 'react-apollo';

import NavButton from './NavButton';
import CartQuery from '../../queries/Etsy/Cart';

export const CartNavButton = props =>
  !props.loading && <NavButton {...props} />;

export default graphql(CartQuery, {
  options: ({ guest_id }) => {
    return { variables: { guest_id } };
  },
})(CartNavButton);
