import React from 'react';

import { NavBannerWrapper } from './NavBarComponents';

const NavBanner = () => {
  return (
    <NavBannerWrapper>
      <span>Candy's Pendant Wrapping</span>
    </NavBannerWrapper>
  );
};

export default NavBanner;
