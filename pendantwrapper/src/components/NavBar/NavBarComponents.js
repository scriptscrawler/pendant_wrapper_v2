import styled from 'styled-components';
import { navSlideIn, slideInBanner, fadeIn } from '../../animations';

const NavBannerWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  color: #cf716f;
  font-size: 66px;
  font-family: 'Handlee', cursive;
  padding-bottom: 2%;

  > img {
    width: auto;
    height: 100px;
    animation: ${fadeIn} 3.5s backwards;
  }

  > span {
    animation: ${slideInBanner} 2s backwards;
    margin: 0 10% 0 10%;
  }
`;

const NavTextWrapper = styled.div`
  display: flex;
  margin-left: 2%;
  min-width: 645px;
  max-width: 25vw;
  justify-content: space-around;
  color: white;
  animation: ${navSlideIn} 2.5s ease-out backwards;

  > img {
    height: 35px;
    align-self: flex-start;
  }
`;

const NavBarWrapper = styled.div`
  background: ${({ bcolor }) => bcolor};
  color: ${({ testProp }) => testProp};
  height: 20%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding-bottom: 0.65%;
`;

const ButtonWrapper = styled.span`
  color: ${({ selected }) => (selected ? '#cf716f;' : `rgb(157, 157, 157)`)};
  ${({ selected }) => selected && 'border-bottom: 2px solid #cf716f;'};
  padding-bottom: 2px;
  transform: scale(1) translate(0, 0);
  transition: all 0.3s;
  font-family: 'Basic', sans-serif;
  font-size: 19px;
  ${({ disabled }) =>
    disabled &&
    'color:white; font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;'}

  > a {
    color: inherit;
    text-decoration: none;
  }
  &:hover {
    ${({ disabled, selected }) => {
      if (!disabled && !selected) {
        return `
            transform: scale(1.1) translate(0, -5px) ;
          `;
      }
    }}
  }

  &:active {
    ${({ disabled, selected }) => {
      if (!disabled && !selected) {
        return `
            transform: scale(1) translate(0, 0) ;
          `;
      }
    }}
  }
`;

const IconWrapper = styled.span`
  color: ${({ selected }) => (selected ? '#cf716f;' : `rgb(157, 157, 157)`)};
  cursor: pointer;
  font-size: 25px;
  transform: scale(1) translate(0, 0);
  transition: all 0.3s;
  margin-right: 3px;
  padding-bottom: 3px;
  ${({ selected }) => selected && 'border-bottom: 2px solid #cf716f;'};

  &:hover {
    ${({ disabled, selected }) => {
      if (!disabled && !selected) {
        return `
              transform: scale(1.1) translate(0, -5px)
          `;
      }
    }}
  }

  &:active {
    ${({ disabled, selected }) => {
      if (!disabled && !selected) {
        return `
            transform: scale(1) translate(0, 0) ;
          `;
      }
    }}
  }

  > a {
    color: inherit;
    text-decoration: none;
  }

  .fam-upper-right {
    transform: scale(0.4);
    top: -6px;
    left: -4px;
  }
`;

const Line = styled.span`
  width: 2px;
  height: 25px;
  background: white;
`;

export {
  NavBarWrapper,
  NavTextWrapper,
  NavBannerWrapper,
  ButtonWrapper,
  Line,
  IconWrapper,
};
