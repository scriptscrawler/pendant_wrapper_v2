import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ButtonWrapper, IconWrapper, Line } from './NavBarComponents';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';

const NavButton = props => {
  const { icon, disabled, selected, text, path, loading, cart } = props;

  return (
    !loading && (
      <Fragment>
        {!icon && (
          <ButtonWrapper disabled={disabled} selected={selected}>
            {!path && text}
            {path && <Link to={path}>{text}</Link>}
          </ButtonWrapper>
        )}
        {icon && (
          <IconWrapper selected={selected}>
            <Link to={path}>
              <div className="fa-layers">
                <FontAwesomeIcon icon={icon} />
                {icon === faCartPlus && cart && (
                  <div
                    className={`${cart && 'fa-layers-counter fam-upper-right'}`}
                  >
                    {cart.listings.length}
                  </div>
                )}
              </div>
            </Link>
          </IconWrapper>
        )}
        {disabled && <Line>&nbsp;</Line>}
      </Fragment>
    )
  );
};

export default NavButton;
