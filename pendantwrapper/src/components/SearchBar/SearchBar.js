import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { PRIMARY_PINK, LIGHT_GRAY, DARK_GRAY } from '../../CSS_SHARED';
import { scaleIn } from '../../animations';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const SearchBarWrapper = styled.div`
  position: relative;
  padding: 6px;
  margin: 35px 25px 0 25px;

  #searchIcon {
    opacity: 0;
    font-size: 18px;
    color: ${LIGHT_GRAY};
    position: relative;
    top: -30px;
    left: 70px;
  }

  #searchIcon:hover {
    ${({ searchTerm }) => {
      return searchTerm === ''
        ? `
        color:red;
        cursor:none;
`
        : `
          color:${PRIMARY_PINK};
          cursor:pointer;
        `;
    }}
  }

  > input {
    border: none;
    border-bottom: solid 2px black;
    color: ${DARK_GRAY};
    font-size: 13px;
    width: 100%;

    &:focus {
      outline: none;
      border-bottom: solid 2px ${LIGHT_GRAY};

      & ~ #searchIcon {
        animation: ${scaleIn} 0.3s forwards;
      }
    }
  }

  > label {
    font-size: 18px;
    color: black;
    position: relative;
    transition: all 0.3s;
    ${({ searchTerm }) => {
      return !searchTerm
        ? `top: -30px;
        left: 0;`
        : `top: -50px;
          left: 0;
          color: ${PRIMARY_PINK};`;
    }}
  }

  > input:focus ~ label {
    top: -50px;
    left: 0;
    color: ${PRIMARY_PINK};
  }
`;

const SearchBar = props => {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = () => {
    if (searchTerm) {
      console.log(searchTerm, 'call search function here');
      setSearchTerm('');
    }
  };

  const handleReturnPressed = ({ key }) => {
    if (key === 'Enter') {
      console.log('Enter Pressed');
      handleSearch();
      document.getElementById('searchInput').blur();
    }
  };

  const handleInputChange = ({ target: { value: searchTerm } }) => {
    setSearchTerm(searchTerm);
  };

  return (
    <SearchBarWrapper searchTerm={searchTerm}>
      <input
        onChange={handleInputChange}
        name="searchInput"
        type="text"
        value={searchTerm}
        id="searchInput"
        onKeyDown={handleReturnPressed}
      />
      <label htmlFor="searchInput">Search</label>
      <FontAwesomeIcon
        onClick={handleSearch}
        icon={faSearch}
        id={'searchIcon'}
      />
    </SearchBarWrapper>
  );
};

export default SearchBar;
