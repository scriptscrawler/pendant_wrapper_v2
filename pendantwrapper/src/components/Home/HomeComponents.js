import styled from 'styled-components';

import { PRIMARY_PINK } from '../../CSS_SHARED';

export const Bar = styled.div`
  background: ${PRIMARY_PINK};
`;

export const RightSideBar = styled(Bar)`
  grid-row: 1 / -1;
  grid-column: rightBar-start / rightBar-end;
`;

export const LeftSideBar = styled(Bar)`
  grid-row: 1 / -1;
  grid-column: leftBar-start / leftBar-end;
`;

export const ListingFiltersWrapper = styled.div`
  background: rgba(black, 0.3);
  grid-column: filterWrapper-start/ filterWrapper-end;
  border-right: 2px solid ${PRIMARY_PINK};

  @media only screen and (max-width: 800px) {
    grid-row: filterBar-start/ filterBar-end;
    grid-column: 1 / -1;
  }
`;
