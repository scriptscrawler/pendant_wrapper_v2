import React from 'react';
import { graphql } from 'react-apollo';

import ListingsQuery from '../../queries/Etsy/ShopListings';
import ListingsManager from '../Listings/';
import ListingFilters from '../Listings/ListingsFilters/ListingFilters';
import {
  RightSideBar,
  LeftSideBar,
  ListingFiltersWrapper,
} from './HomeComponents';
import { MainDisplayWrapper } from '../Shared';

const Home = props => {
  const {
    data: { shoplistings, loading },
  } = props;
  // console.log('Home Props', props);
  // console.log('Home Props', shoplistings);

  return (
    !loading && (
      <MainDisplayWrapper>
        <RightSideBar />
        <LeftSideBar />
        <ListingFiltersWrapper>
          <ListingFilters listings={shoplistings} />
        </ListingFiltersWrapper>
        {/* <ListingsManager listings={shoplistings} /> */}
      </MainDisplayWrapper>
    )
  );
};

export default graphql(ListingsQuery)(Home);
