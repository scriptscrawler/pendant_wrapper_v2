import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import React from 'react';
import { shallow } from 'enzyme';

import Home from '../Home';

describe('Home unit tests ', () => {
  const defaultProps = {};

  const shallowRenderHomeComp = (Comp, props = defaultProps) => {
    const mergedProps = {
      ...defaultProps,
      ...props,
    };

    return shallow(<Comp {...mergedProps} />);
  };
  test('should render component', () => {
    const HomeComp = shallowRenderHomeComp(Home);

    expect(HomeComp.exists()).toEqual(true);
  });
});
