import React from 'react';

import Accordion from '../../Accordion/Accordion';
import { ACCORDION_CONFIG } from '../../../constants';
import { FiltersWrapper } from './ListingsFiltersComponents';
import { generateListsForFiltersBar } from './ListingFilters_L';

const ListingFilters = props => {
  const { listings } = props;

  const filtersConfig = generateListsForFiltersBar(listings);

  return (
    <FiltersWrapper>
      <Accordion
        accordionConfig={ACCORDION_CONFIG}
        filtersConfig={filtersConfig}
      />
    </FiltersWrapper>
  );
};

export default ListingFilters;
