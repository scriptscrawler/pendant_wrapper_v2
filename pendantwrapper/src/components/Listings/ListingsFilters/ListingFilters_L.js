export const generateListsForFiltersBar = listings => {
  const starterObject = {
    category_path: [],
    materials: [],
    quantity: [],
    views: [],
  };

  const { category_path, materials, quantity, views } = listings.reduce(
    (filtersData, listing) => {
      filtersData.category_path.push(...listing.category_path);
      filtersData.materials.push(...listing.materials);
      filtersData.quantity.push(listing.quantity);
      filtersData.views.push(listing.views);

      return filtersData;
    },
    starterObject
  );
  const dateFilters = ['Newest', 'Oldest'];
  const categoryFilters = [...new Set(category_path)];
  const materialsFilters = [...new Set(materials)];
  const quantityFilters = [...new Set(quantity)];
  const viewsFilters = [...new Set(views)];

  return [
    dateFilters,
    categoryFilters,
    materialsFilters,
    quantityFilters,
    viewsFilters,
  ];
};
