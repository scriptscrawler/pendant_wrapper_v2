import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { ButtonWrapper } from './ListingFilterButtonComponents';

const ListingsFilterButton = props => {
  const {
    handleSelectedFilterNameState,
    selected,
    buttonText,
    uniqueId,
    parentId,
  } = props;
  return (
    <div>
      <ButtonWrapper
        onClick={() => handleSelectedFilterNameState(parentId, uniqueId)}
        selected={selected}
      >
        {buttonText}
        {selected && <FontAwesomeIcon icon={faCheckCircle} />}
      </ButtonWrapper>
    </div>
  );
};

export default ListingsFilterButton;
