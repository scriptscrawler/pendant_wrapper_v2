import styled from 'styled-components';

export const FiltersWrapper = styled.div`
  color: black;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  @media only screen and (max-width: 800px) {
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
  }
`;
