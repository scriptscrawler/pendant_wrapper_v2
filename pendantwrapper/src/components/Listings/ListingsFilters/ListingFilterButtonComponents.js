import styled, { keyframes, css } from 'styled-components';

import { PRIMARY_PINK_RGB, PRIMARY_PINK } from '../../../CSS_SHARED';

const hoverFilterAnimation = keyframes`
    0%{
    background: rgba(${PRIMARY_PINK_RGB}, 0.2);

    }
    100%{
        background: rgba(${PRIMARY_PINK_RGB}, 0.9);

    }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  // font-size: 14px;
  font-family: 'Basic', sans-serif;
  font-size: 19px;
  border-radius: 100px;
  padding: 8px 25px;
  margin-top: 8px;
  margin-bottom: 8px;
  white-space: nowrap;
  border: 1px solid ${({ selected }) => (selected ? 'black' : 'transparent')};
  background: ${({ selected }) => (selected ? 'black' : 'white')};
  color: ${({ selected }) => (selected ? PRIMARY_PINK : 'rgb(157, 157, 157)')};

  ${({ selected }) =>
    !selected &&
    css`
      &:hover {
        animation: ${hoverFilterAnimation} 1s forwards;
        border: 1px solid rgba(0, 0, 0, 0.3);
        color: black;
      }
    `}

  > svg {
    color: rgb(${PRIMARY_PINK_RGB});
    margin-left: 15px;
  }
`;
