import React, { Component } from 'react';
import styled from 'styled-components';
import { PRIMARY_PINK } from '../../CSS_SHARED';
import Accordion from '../Accordion/Accordion';
import ListingsFilterButton from '../ListingsFilterButton';
import {
  faArrowDown,
  faArrowUp,
  faCaretUp,
  faCaretDown,
} from '@fortawesome/free-solid-svg-icons';
import SearchBar from '../SearchBar';

const FiltersWrapper = styled.div`
  color: black;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

class ListingFilters extends Component {
  constructor(props) {
    super(props);

    this.accordionConfig = [
      {
        title: 'By Date',
        iconClosed: faCaretDown,
        iconOpen: faCaretUp,
      },
      {
        title: 'By Category',
        iconClosed: faCaretDown,
        iconOpen: faCaretUp,
      },
      {
        title: 'By Materials',
        iconClosed: faCaretDown,
        iconOpen: faCaretUp,
      },
      {
        title: 'By Quantity',
        iconClosed: faCaretDown,
        iconOpen: faCaretUp,
      },
      {
        title: 'By Views',
        iconClosed: faCaretDown,
        iconOpen: faCaretUp,
      },
    ];
  }
  generateListsForFiltersBar() {
    const { listings } = this.props;

    const starterObject = {
      category_path: [],
      materials: [],
      quantity: [],
      views: [],
    };

    const { category_path, materials, quantity, views } = listings.reduce(
      (filtersData, listing) => {
        filtersData.category_path.push(...listing.category_path);
        filtersData.materials.push(...listing.materials);
        filtersData.quantity.push(listing.quantity);
        filtersData.views.push(listing.views);

        return filtersData;
      },
      starterObject
    );
    const dateFilters = ['Newest', 'Oldest'];
    const categoryFilters = [...new Set(category_path)];
    const materialsFilters = [...new Set(materials)];
    const quantityFilters = [...new Set(quantity)];
    const viewsFilters = [...new Set(views)];

    return [
      dateFilters,
      categoryFilters,
      materialsFilters,
      quantityFilters,
      viewsFilters,
    ];
  }
  render() {
    const filtersConfig = this.generateListsForFiltersBar();

    return (
      <FiltersWrapper>
        <SearchBar />
        <Accordion
          accordionConfig={this.accordionConfig}
          filtersConfig={filtersConfig}
        />
      </FiltersWrapper>
    );
  }
}

export default ListingFilters;
//category_path, materials, quantity, views, price
