import React, { Fragment } from 'react';

import ListingsFilterButton from '../ListingsFilterButton';

export const ListingFilterButtonManager = props => {
  const {
    filterNames,
    selectedFilterNames,
    parentId,
    handleSelectedFilterNameState,
  } = props;
  return (
    <Fragment>
      {filterNames.map((filterName, uniqueId) => {
        return (
          <ListingsFilterButton
            key={`${uniqueId}-listingFilterButton`}
            selected={selectedFilterNames[uniqueId] === 0 ? false : true}
            buttonText={
              isNaN(filterName)
                ? `${filterName.slice(0, 1).toUpperCase()}${filterName.slice(
                    1
                  )}`
                : filterName
            }
            uniqueId={uniqueId}
            parentId={parentId}
            handleSelectedFilterNameState={handleSelectedFilterNameState}
          />
        );
      })}
    </Fragment>
  );
};
