import React from 'react';
import styled from 'styled-components';

import Listing from './Listing';

const ListingWrapper = styled.div`
  grid-column: grid-start / grid-end;
  background: white;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
  grid-template-rows: auto;
  grid-gap: 10em 10em;
  padding: 5em;

  @media only screen and (max-width: 800px) {
    grid-row: otherContent-start / otherContent-end;
  }
`;

const ListingsManager = props => {
  const { listings } = props;
  if (listings) {
    return (
      <ListingWrapper>
        {listings.slice(0, 3).map(listing => {
          return <Listing key={listing.listing_id} {...listing} />;
        })}
      </ListingWrapper>
    );
  }
  return null;
};

export default ListingsManager;
