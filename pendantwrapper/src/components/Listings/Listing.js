import React from 'react';
import { graphql } from 'react-apollo';

import ListingImageQuery from '../../queries/Etsy/ListingImages';
import { ListingWrapper } from './ListingComponents';

const Listing = props => {
  console.log('Listing props', props);
  const {
    data: { loading, listingimages },
  } = props;

  return (
    !loading && (
      <ListingWrapper>
        {listingimages &&
          listingimages.map(image => {
            return <img key={image.listing_image_id} src={image.url_75x75} />;
          })}
      </ListingWrapper>
    )
  );
};

export default graphql(ListingImageQuery, {
  options: ({ listing_id }) => {
    listing_id = listing_id.toString();

    return { variables: { listing_id } };
  },
})(Listing);
