import React from 'react';

import { MainWrapper } from './MainDisplayWrapperComponents';

const MainDisplayWrapper = props => <MainWrapper>{props.children}</MainWrapper>;

export default MainDisplayWrapper;
