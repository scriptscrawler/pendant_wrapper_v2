import React from 'react';
import { shallow } from 'enzyme';

import { MainWrapper } from '../MainDisplayWrapperComponents';

describe('MainDisplayWrapperComponent Unit Tests ', () => {
  describe('MainWrapper', () => {
    test('should render MainWrapper with correct css', () => {
      const MainWrapperComp = shallow(<MainWrapper />);

      expect(MainWrapperComp).toMatchSnapshot();
    });
  });
});
