import React from 'react';

import MainDisplaYWrapper from '../MainDisplayWrapper';
import { enzymeRenderCreator } from '../../../../setupTests';
import { MainWrapper } from '../MainDisplayWrapperComponents';

describe('MainDisplayWrapper unit tests', () => {
  const defaultProps = {
    children: (
      <div>
        <p>Hi</p>
      </div>
    ),
  };

  const MainDisplayRenderer = new enzymeRenderCreator(
    MainDisplaYWrapper,
    defaultProps
  );

  let MainDisplayComp;

  beforeEach(() => {
    MainDisplayComp = MainDisplayRenderer.shallowRender();
  });

  test('should render a MainWrapper', () => {
    const MainWrapperComp = MainDisplayComp.find(MainWrapper);

    expect(MainWrapperComp.exists()).toEqual(true);
  });

  test('should render the correct children', () => {
    const MainWrapperCompChildren = MainDisplayComp.find(MainWrapper).props(
      'children'
    ).children;

    const expectedChildren = (
      <div>
        <p>Hi</p>
      </div>
    );

    expect(MainWrapperCompChildren).toEqual(expectedChildren);
  });
});
