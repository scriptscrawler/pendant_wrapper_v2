import styled from 'styled-components';

import { PRIMARY_PINK } from '../../../CSS_SHARED';

export const MainWrapper = styled.div`
  display: grid;
  grid-template-columns:
    [leftBar-start] 1% [leftBar-end filterWrapper-start] repeat(2, 1fr)
    [filterWrapper-end grid-start] repeat(9, 1fr)
    [grid-end rightBar-start] 1% [rightBar-end];
  height: 100vh;
  border-top: 2px solid ${PRIMARY_PINK};
  border-right: 2px solid ${PRIMARY_PINK};

  @media only screen and (max-width: 800px) {
    grid-template-columns:
      [leftBar-start] 1% [leftBar-end grid-start] repeat(9, 1fr)
      [grid-end rightBar-start] 1% [rightBar-end];

    grid-template-rows: [filterBar-start] repeat(2, 1fr) [filterBar-end otherContent-start] 1fr [otherContent-end];
  }
`;
