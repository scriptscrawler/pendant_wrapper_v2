import React from 'react';
import styled from 'styled-components';

import { PRIMARY_PINK } from '../../CSS_SHARED';

const MainWrapper = styled.div`
  display: grid;
  grid-template-columns: 1% repeat(12, 1fr) 1%;
  grid-template-rows: 0.5% repeat(12, 1fr);
  height: 100vh;
  border-top: 2px solid ${PRIMARY_PINK};
  border-right: 2px solid ${PRIMARY_PINK};
`;

const MainDisplayWrapper = props => <MainWrapper>{props.children}</MainWrapper>;

export default MainDisplayWrapper;
