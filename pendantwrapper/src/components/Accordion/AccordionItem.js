import React from 'react';
import styled, { keyframes, css } from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { PRIMARY_PINK } from '../../CSS_SHARED';
import { ListingFilterButtonManager } from '../Listings/ListingsFilters/ListingFilterButtonManager';

const genContentAnimationOpen = height => keyframes`
    0% {
        height:0px;
    }
    100% {
        height: ${height + 5}px;
    }
`;

const genContentAnimationClosed = height => keyframes`
    0% {
        height: ${height + 5}px;
    }
    100% {
        height:0px;
    }
    
`;

const AccordionParentWrapper = styled.div`
    

 
  div[id='main-content-${({ uniqueId }) => uniqueId}'] {
    height:${({ toggled, mainContentHeight }) =>
      toggled ? mainContentHeight : 0}px;
      max-height:350px;
    display: flex;
    flex-direction: column;
    align-items: center;
    flex: 1;
    overflow: hidden;
    overflow-y:${({ mainContentHeight }) =>
      mainContentHeight > 350 ? 'scroll' : 'none'};
    ${({ toggled }) =>
      toggled
        ? css`
            animation: ${({ mainContentHeight }) =>
                genContentAnimationOpen(mainContentHeight)}
              0.5s forwards;
          `
        : css`
            animation: ${({ mainContentHeight }) =>
                genContentAnimationClosed(mainContentHeight)}
              0.5s forwards;
          `}

          &::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 7px;
        }
        &::-webkit-scrollbar-thumb {
            border-radius: 4px;
            background-color: rgba(0,0,0,.5);
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
        }
  }
    
`;

const AccordionTitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 6px;
  font-size: 18px;
  margin: 25px 25px 25px 25px;
  border-bottom: 1.5px solid black;

  > span {
    white-space: nowrap;
  }

  .fa-layers-counter {
    background: ${PRIMARY_PINK};
    color: black;
  }
`;

const IconWrapper = styled.div`
  margin-left: 3%;
`;

const AccordionItem = props => {
  const {
    config: { title, iconOpen, iconClosed },
    uniqueId,
    toggleState,
    accordionStateHandler,
    filterData,
    selectedFilterNames,
    handleSelectedFilterNameState,
    mainContentHeight,
  } = props;

  const selectedFiltersCount = selectedFilterNames.filter(n => n !== 0).length;

  return (
    <AccordionParentWrapper
      mainContentHeight={mainContentHeight}
      uniqueId={uniqueId}
      toggled={toggleState}
    >
      <AccordionTitleWrapper
        selectedFiltersCount={selectedFiltersCount}
        onClick={() => accordionStateHandler(uniqueId)}
      >
        <span>
          {title}
          {selectedFiltersCount > 0 && (
            <div className="fa-layers">
              <div className="fa-layers-counter fa-2x">
                {selectedFiltersCount}
              </div>
            </div>
          )}
        </span>
        {iconOpen && iconClosed && (
          <IconWrapper>
            <FontAwesomeIcon
              size="1x"
              icon={!toggleState ? iconClosed : iconOpen}
            />
          </IconWrapper>
        )}
      </AccordionTitleWrapper>

      <div id={`main-content-${uniqueId}`}>
        <ListingFilterButtonManager
          filterNames={filterData}
          parentId={uniqueId}
          selectedFilterNames={selectedFilterNames}
          handleSelectedFilterNameState={handleSelectedFilterNameState}
        />
      </div>
    </AccordionParentWrapper>
  );
};

export default AccordionItem;
