import React, { useState } from 'react';
import styled from 'styled-components';
import AccordionItem from './AccordionItem';
import SearchBar from '../SearchBar';

const AccordionWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(175px, 1fr));
  overflow: hidden;

  @media only screen and (max-width: 800px) {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    width: 98.4%;
    margin-left: 0.5%;

    border-bottom: 3px solid rgba(0, 0, 0, 0.5);
  }
`;

const Accordion = props => {
  const { filtersConfig, accordionConfig } = props;

  const [selectedFilterNames, setFilterName] = useState(
    props.filtersConfig.map(item => Array(item.length).fill(0))
  );
  const [accordionToggleState, setAccordionToggleState] = useState(
    Array(props.accordionConfig.length).fill({
      height: 0,
      toggled: 0,
    })
  );

  const accordionStateHandler = index => {
    const copiedToggleState = [...accordionToggleState];
    const newToggledState = copiedToggleState[index].toggled === 0 ? 1 : 0;
    const contentHeight =
      document.getElementById(`main-content-${index}`).scrollHeight + 5;

    copiedToggleState[index] = {
      height: contentHeight,
      toggled: newToggledState,
    };

    setAccordionToggleState(copiedToggleState);
  };

  //TODO: need to find a way to rea this map and get filter names
  const generateSelectedFilterNames = currentState => {
    const filterNameMap = {
      0: 'date',
      1: 'category',
      2: 'materials',
      3: 'quantity',
      4: 'views',
    };
  };

  const handleSelectedFilterNameState = (parentIndex, childIndex) => {
    const copyOfCurrentFilterState = [...selectedFilterNames];

    let newStateVal;
    if (parentIndex !== 0) {
      newStateVal =
        copyOfCurrentFilterState[parentIndex][childIndex] === 0 ? 1 : 0;
      copyOfCurrentFilterState[parentIndex][childIndex] = newStateVal;
    } else {
      newStateVal = childIndex === 0 ? [1, 0] : [0, 1];
      copyOfCurrentFilterState[parentIndex] = newStateVal;
    }

    console.log('call filter function here with ', copyOfCurrentFilterState);
    setFilterName(copyOfCurrentFilterState);
  };

  return (
    <AccordionWrapper>
      <SearchBar />
      {accordionConfig.map((accordionItem, uniqueId) => {
        return (
          <AccordionItem
            accordionStateHandler={accordionStateHandler}
            config={accordionItem}
            key={`${uniqueId}-accordionItem`}
            uniqueId={uniqueId}
            filterData={filtersConfig[uniqueId]}
            toggleState={
              accordionToggleState[uniqueId].toggled === 0 ? false : true
            }
            mainContentHeight={accordionToggleState[uniqueId].height}
            selectedFilterNames={selectedFilterNames[uniqueId]}
            handleSelectedFilterNameState={handleSelectedFilterNameState}
          />
        );
      })}
    </AccordionWrapper>
  );
};

export default Accordion;
