const AppTheme = {
  PendantPink: '#cf716f',
  mainBannerFontSize: '66px',
  navFontSize: '66px',
  navSelectedColor: '#cf716f',
  navStandardColor: '#9d9d9d',
  storeTextColor: 'white',
  navIconFontSize: '30px',
  featureTagLineFont: null,
  testProp: 'red',
};

export default AppTheme;
