import axios from 'axios';

export const setLocalEtsyGuestData = userData => {
  localStorage.setItem('pendant_data', JSON.stringify(userData));
};

export const getNewEtsyGuestId = receivedEtsyGuestId => {
  axios
    .get('/etsy/guestid')
    .then(({ data }) => {
      receivedEtsyGuestId(data);
      setLocalEtsyGuestData(data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const retrieveLocalEtsyGuestData = () => {
  const retrievedData = localStorage.getItem('pendant_data');
  return retrievedData ? JSON.parse(retrievedData) : retrievedData;
};

export const manageEtsyGuestData = (receivedEtsyGuestId, etsyGuestCartData) => {
  if (!etsyGuestCartData.guest_id) {
    const guestCartData = retrieveLocalEtsyGuestData();

    guestCartData
      ? receivedEtsyGuestId(guestCartData)
      : getNewEtsyGuestId(receivedEtsyGuestId);
  }
};
