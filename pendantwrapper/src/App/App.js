import React, { useEffect, Fragment } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Home from '../components/Home';
import NavBar from '../components/NavBar';
import { receivedEtsyGuestId } from '../actions/etsyguestid';
import { manageEtsyGuestData } from './App_L';

export const Working = () => <div>Working</div>;

export const App = props => {
  const { etsyGuestCartData, receivedEtsyGuestId } = props;

  const { guest_id } = etsyGuestCartData;

  if (!guest_id) {
    useEffect(() => {
      console.log('use effect called');
      manageEtsyGuestData(receivedEtsyGuestId, etsyGuestCartData);
    });
  }

  return guest_id ? (
    <Fragment>
      <Router>
        <div>
          <NavBar guest_id={guest_id} />
          <Route exact path="/" render={() => <Redirect to="/home" />} />
          <Route exact path="/home" component={Home} />
          <Route path="/about" component={Working} />
          <Route path="/contact" component={Working} />
          <Route path="/classes" component={Working} />
          <Route path="/cart" component={Working} />
          <Route path="/settings" component={Working} />
        </div>
      </Router>
    </Fragment>
  ) : null;
};

const mapStateToProps = ({ etsyGuestCartData: { etsyGuestCartData } }) => ({
  etsyGuestCartData,
});

const mapDispatchToProps = {
  receivedEtsyGuestId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
