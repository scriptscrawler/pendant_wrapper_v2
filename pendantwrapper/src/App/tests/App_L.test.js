import axios from 'axios';

import {
  setLocalEtsyGuestData,
  getNewEtsyGuestId,
  retrieveLocalEtsyGuestData,
  manageEtsyGuestData,
} from '../App_L';

describe('App Logic File Unit Tests', () => {
  describe('setLocalEtsyGuestData', () => {
    const setItemStub = jest.fn();

    test('should call setItem', () => {
      window.localStorage.setItem = setItemStub;
      setLocalEtsyGuestData({ someData: true });

      expect(setItemStub).toHaveBeenCalledTimes(1);
    });

    test.skip('should call setItem with correct args', () => {
      window.localStorage.setItem = setItemStub;
      setLocalEtsyGuestData({ someData: true });

      const expectedArgs = ['pendant_data', JSON.stringify({ someData: true })];

      expect(setItemStub.mock.calls[0][0]).toEqual(expectedArgs);
    });
  });

  describe('getNewEtsyGuestId', () => {
    // const getStub = jest.fn(() => Promise.resolve({ someData: true }));
    // axios.get = getStub;

    test.skip('should make the request to the correct url', () => {
      const expectedUrl = '/etsy/guestid';

      expect(axios.get.mock.calls[0][0]).toEqual(expectedUrl);
    });
    describe('when the promise resolves ', () => {
      test.skip('should call receivedEtsyGuestId', () => {});

      test.skip('should call receivedEtsyGuestId with correct args', () => {});

      test.skip('should call setLocalEtsyGuestData', () => {});

      test.skip('should call setLocalEtsyGuestData with correct args', () => {});
    });

    describe('when the promise rejects ', () => {
      test.skip('should consolelog error', () => {});
    });
  });

  describe('retrieveLocalEtsyGuestData', () => {
    test.skip('should call getItem', () => {});

    test.skip('should call getItem with correct args', () => {});

    describe('when retrieved data is truthy', () => {
      test.skip('should return the correct data', () => {});
    });

    describe('when retrieved data is falsy', () => {
      test.skip('should return the correct data', () => {});
    });
  });

  describe('manageEtsyGuestData', () => {
    describe('when guest id is truthy', () => {
      test.skip('should not call retrieveLocalEtsyGuestData', () => {});

      test.skip('should not call receivedEtsyGuestId', () => {});

      test.skip('should not call getNewEtsyGuestId', () => {});
    });

    describe('when guest id is falsy', () => {
      test.skip('should call retrieveLocalEtsyGuestData', () => {});
      describe('when guestCartData is truthy', () => {
        test.skip('should call receivedEtsyGuestId', () => {});

        test.skip('should call receivedEtsyGuestId with correct args', () => {});
      });

      describe('when guestCartData is truthy', () => {
        test.skip('should call getNewEtsyGuestId', () => {});

        test.skip('should call getNewEtsyGuestId with correct args', () => {});
      });
    });
  });
});
