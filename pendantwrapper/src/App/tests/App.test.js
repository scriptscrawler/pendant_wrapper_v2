import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import { App } from '../App';
import * as AppLogic from '../App_L';
import { enzymeRenderCreator } from '../../setupTests';
import Home from '../../components/Home';
import { Working } from '../App';
import NavBar from '../../components/NavBar';
import { faUmbrella } from '@fortawesome/free-solid-svg-icons';

describe('App component unit tests', () => {
  const receivedEtsyGuestIdStub = jest.fn();

  const defaultProps = {
    receivedEtsyGuestId: receivedEtsyGuestIdStub,
    etsyGuestCartData: {
      checkout_url:
        'https://www.etsy.com/apps/355744829986/carts/ir1rtmm3rfhfs/checkout?utm_source=pendantwrapper&utm_medium=api&utm_campaign=api',
      guest_id: 'ir1rtmm3rfhfs',
    },
  };
  describe('Rendering ', () => {
    const manageEtsyGuestDataStub = jest.fn();
    AppLogic.manageEtsyGuestData = manageEtsyGuestDataStub;

    const AppCompRenderer = new enzymeRenderCreator(App, defaultProps);

    describe('when there is a guest_id', () => {
      let AppComp;

      beforeEach(() => {
        AppComp = AppCompRenderer.shallowRender();
      });

      test('should not call manageEtsyGuestData', () => {
        expect(manageEtsyGuestDataStub).toHaveBeenCalledTimes(0);
      });

      test('should render Router component', () => {
        const RouterComp = AppComp.find(Router);

        expect(RouterComp.exists()).toEqual(true);
      });

      test('should render 7 route components', () => {
        const RoutesComps = AppComp.find(Route);

        expect(RoutesComps.length).toEqual(7);
      });

      test('should provide the correct props to route 1', () => {
        const RouteProps = AppComp.find(Route)
          .at(0)
          .props();

        const expectedProps = {
          exact: true,
          path: '/',
          render: expect.any(Function),
        };

        expect(RouteProps).toEqual(expectedProps);
      });

      test('should provide the correct render prop to route one', () => {
        const renderFunction = AppComp.find(Route)
          .at(0)
          .prop('render');

        const RedirectComp = renderFunction();

        expect(RedirectComp).toEqual(<Redirect to="/home" />);
      });

      test('should provide the correct props to route 2', () => {
        const RouteProps = AppComp.find(Route)
          .at(1)
          .props();

        const expectedProps = {
          exact: true,
          path: '/home',
          component: Home,
        };

        expect(RouteProps).toEqual(expectedProps);
      });

      test('should provide the correct props to route 3', () => {
        const RouteProps = AppComp.find(Route)
          .at(2)
          .props();

        const expectedProps = {
          path: '/about',
          component: expect.any(Function),
        };

        expect(RouteProps).toEqual(expectedProps);
      });

      test('should provide the correct props to route 4', () => {
        const RouteProps = AppComp.find(Route)
          .at(3)
          .props();

        const expectedProps = {
          path: '/contact',
          component: expect.any(Function),
        };

        expect(RouteProps).toEqual(expectedProps);
      });

      test('should provide the correct props to route 5', () => {
        const RouteProps = AppComp.find(Route)
          .at(4)
          .props();

        const expectedProps = {
          path: '/classes',
          component: expect.any(Function),
        };

        expect(RouteProps).toEqual(expectedProps);
      });

      test('should provide the correct props to route 6', () => {
        const RouteProps = AppComp.find(Route)
          .at(5)
          .props();

        const expectedProps = {
          path: '/cart',
          component: expect.any(Function),
        };

        expect(RouteProps).toEqual(expectedProps);
      });

      test('should provide the correct props to route 7', () => {
        const RouteProps = AppComp.find(Route)
          .at(6)
          .props();

        const expectedProps = {
          path: '/settings',
          component: expect.any(Function),
        };

        expect(RouteProps).toEqual(expectedProps);
      });

      test('should render NavBar component', () => {
        const NavBarComp = AppComp.find(NavBar);

        expect(NavBarComp.exists()).toEqual(true);
      });

      test('should render NavBar component with correct props', () => {
        const NavBarCompProps = AppComp.find(NavBar).props();

        const expectedprops = {
          guest_id: 'ir1rtmm3rfhfs',
        };

        expect(NavBarCompProps).toMatchObject(expectedprops);
      });
    });

    describe('when there is no guest id', () => {
      const newProps = {
        receivedEtsyGuestId: receivedEtsyGuestIdStub,
        etsyGuestCartData: {
          checkout_url: null,
          guest_id: null,
        },
      };
      const AppCompRenderer = new enzymeRenderCreator(App, newProps);

      let AppComp;

      beforeEach(() => {
        AppComp = AppCompRenderer.shallowRender({
          etsyGuestCartData: {
            checkout_url: null,
            guest_id: null,
          },
        });
      });

      test.skip('should call manageEtsyGuestData', () => {
        expect(manageEtsyGuestDataStub).toHaveBeenCalledTimes(1);
      });

      test.skip('should call manageEtsyGuestData with correct args', () => {});

      test.skip('should return null', () => {});
    });
  });
});
