import AppTheme from '../AppTheme';

describe('AppTheme unit tests ', () => {
  test('should return the correct theme', () => {
    expect(AppTheme).toMatchSnapshot();
  });
});
