import gql from 'graphql-tag';

export default gql`
  mutation RemoveFromCart($listing_id: String, $guest_id: String) {
    remove_from_cart(listing_id: $listing_id, guest_id: $guest_id) {
      listings {
        listing_id
        state
        user_id
        category_id
        title
        description
        price
        currency_code
        quantity
        shop_section_id
        views
        shipping_template_id
        processing_min
        processing_max
      }
    }
  }
`;
