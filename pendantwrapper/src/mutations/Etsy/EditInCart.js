import gql from 'graphql-tag';

export default gql`
  mutation EditListingQuantity(
    $listing_id: String
    $guest_id: String
    $quantity: String
  ) {
    edit_cart_listing_quantity(
      listing_id: $listing_id
      guest_id: $guest_id
      quantity: $quantity
    ) {
      listings {
        listing_id
        state
        user_id
        category_id
        title
        description
        price
        currency_code
        quantity
        shop_section_id
        views
        shipping_template_id
        processing_min
        processing_max
      }
    }
  }
`;
