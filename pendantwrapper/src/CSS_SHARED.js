export const PRIMARY_PINK = '#cf716f';
export const PRIMARY_PINK_RGB = '207, 113, 111';
export const SUCESS_GREEN_RGB = '0, 255, 46';
export const LIGHT_GRAY = 'lightgray';
export const DARK_GRAY = 'darkgray';
