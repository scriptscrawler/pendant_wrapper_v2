import {
  faCartPlus,
  faCog,
  faCaretUp,
  faCaretDown,
} from '@fortawesome/free-solid-svg-icons';

/* Navigation Map */
export const PATH_MAP = {
  '/': 1,
  '/home': 1,
  '/about': 2,
  '/contactme': 3,
  '/classes': 4,
  '/cart': 5,
  '/settings': 6,
};

/* Nav Button Map */
export const NAV_BUTTON_CONFIG = [
  { text: 'Pendant Wrapper', icon: null, path: null, disabled: true },
  {
    text: 'Home',
    icon: null,
    path: '/home',
    disabled: false,
  },
  { text: 'About', icon: null, path: '/about', disabled: false },
  { text: 'Contact Me', icon: null, path: '/contactme', disabled: false },
  { text: 'Classes', icon: null, path: '/classes', disabled: false },
  { text: null, icon: faCartPlus, path: '/cart', disabled: false },
  // { text: null, icon: faCog, path: '/settings', disabled: false },
];

/* FIltersBar Accordion Config */
export const ACCORDION_CONFIG = [
  {
    title: 'By Date',
    iconClosed: faCaretDown,
    iconOpen: faCaretUp,
  },
  {
    title: 'By Category',
    iconClosed: faCaretDown,
    iconOpen: faCaretUp,
  },
  {
    title: 'By Materials',
    iconClosed: faCaretDown,
    iconOpen: faCaretUp,
  },
  {
    title: 'By Quantity',
    iconClosed: faCaretDown,
    iconOpen: faCaretUp,
  },
  {
    title: 'By Views',
    iconClosed: faCaretDown,
    iconOpen: faCaretUp,
  },
];
