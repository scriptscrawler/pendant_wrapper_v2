import { keyframes, css } from 'styled-components';

export const navSlideIn = keyframes`
  0% {
    transform: translateX(-100%);
    opacity: 0;
  }
  70% {
    transform: translateX(0);
    opacity: 0.5;
  }
  100% {
    opacity: 1;
  }
`;

export const navSlideOut = keyframes`
  0% {
    transform: translateX(0);
    opacity: 1;
  }
  60% {
    opacity: 0;
  }
  100% {
    opacity: 0;
    transform: translateX(-100%);
    margin-bottom: 0px;
  }
`;

export const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;
export const fadeOut = keyframes`
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
`;

export const slideInBanner = keyframes`
0%{
    transform:translateY(-100%);
    opacity:0;
}
100%{
    transform:translateY(0, 0);
    opacity:1;
    
}
`;

export const scaleIn = keyframes`
0%{
    transform:scale(0);
    opacity:0;
}
100%{
    transform:scale(1);
    opacity:1;
    
}
`;
