import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleWare from 'redux-thunk';
import { ThemeProvider } from 'styled-components';

import theme from './App/AppTheme.js';
import App from './App/App';
import './index.css';
import * as serviceWorker from './serviceWorker';
import reducers from './reducers/RootReducer';

const client = new ApolloClient({ uri: '/graphql' });

const loggerMiddleWare = createLogger();
const store = createStore(
  reducers,
  applyMiddleware(thunkMiddleWare, loggerMiddleWare)
);

ReactDOM.render(
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <App />
      </Provider>
    </ThemeProvider>
  </ApolloProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();
