import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import 'jest-styled-components';
import 'jest-enzyme';

import { createSerializer } from 'enzyme-to-json';

expect.addSnapshotSerializer(createSerializer({ mode: 'deep' }));

export class enzymeRenderCreator {
  constructor(Comp, defaultProps) {
    this.Comp = Comp;
    this.defaultProps = defaultProps || {};
  }

  shallowRender(props) {
    const mergedProps = {
      ...this.defaultProps,
      ...props,
    };
    return shallow(<this.Comp {...mergedProps} />);
  }

  deepRender(props) {
    const mergedProps = {
      ...this.defaultProps,
      ...props,
    };
    return mount(<this.Comp {...mergedProps} />);
  }

  renderRender(props) {
    const mergedProps = {
      ...this.defaultProps,
      ...props,
    };
    return render(<this.Comp {...mergedProps} />);
  }
}
