import { RECEIVED_ETSY_GUEST_ID } from './types';

export const receivedEtsyGuestId = payload => ({
  type: RECEIVED_ETSY_GUEST_ID,
  payload,
});
