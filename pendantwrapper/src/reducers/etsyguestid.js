import { createReducer } from './utils';

import { RECEIVED_ETSY_GUEST_ID } from '../actions/types';

export const STATE_KEY = 'etsyGuestCartData';

const defaultState = {
  etsyGuestCartData: { checkout_url: null, guest_id: null },
};

export const handleReceivedEtsyGuestId = (
  state,
  { payload: etsyGuestCartData }
) => ({
  ...state,
  etsyGuestCartData,
});

export default createReducer(defaultState, {
  RECEIVED_ETSY_GUEST_ID: handleReceivedEtsyGuestId,
});
