import { combineReducers } from 'redux';
import EtsyGuestIdReducer, { STATE_KEY as EtsyGuestIdKey } from './etsyguestid';

const rootReducer = combineReducers({
  [EtsyGuestIdKey]: EtsyGuestIdReducer,
});

export default rootReducer;
