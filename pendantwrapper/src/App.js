import React, { useEffect, Fragment } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';

import Home from './components/Home';
import NavBar from './components/NavBar';
import { receivedEtsyGuestId } from './actions/etsyguestid';

const Working = () => <div>Working</div>;

const setLocalEtsyGuestData = userData => {
  localStorage.setItem('pendant_data', JSON.stringify(userData));
};

const getNewEtsyGuestId = receivedEtsyGuestId => {
  axios
    .get('/etsy/guestid')
    .then(({ data }) => {
      receivedEtsyGuestId(data);
      setLocalEtsyGuestData(data);
    })
    .catch(err => {
      console.log(err);
    });
};

const retrieveLocalEtsyGuestData = () => {
  const retrievedData = localStorage.getItem('pendant_data');
  return retrievedData ? JSON.parse(retrievedData) : retrievedData;
};

const manageEtsyGuestData = (receivedEtsyGuestId, etsyGuestCartData) => {
  if (!etsyGuestCartData.guest_id) {
    const guestCartData = retrieveLocalEtsyGuestData();
    if (!etsyGuestCartData.guest_id) {
      guestCartData
        ? receivedEtsyGuestId(guestCartData)
        : getNewEtsyGuestId(receivedEtsyGuestId);
    }
  }
};

const App = props => {
  const { etsyGuestCartData, receivedEtsyGuestId } = props;

  const { guest_id } = etsyGuestCartData;

  useEffect(
    () => {
      manageEtsyGuestData(receivedEtsyGuestId, etsyGuestCartData);
    },
    [receivedEtsyGuestId, etsyGuestCartData]
  );

  return guest_id ? (
    <Fragment>
      <Router>
        <div>
          <NavBar guest_id={guest_id} />
          <Route exact path="/" render={() => <Redirect to="/home" />} />
          <Route exact path="/home" component={Home} />
          <Route path="/abour" component={Working} />
          <Route path="/contact" component={Working} />
          <Route path="/classes" component={Working} />
          <Route path="/cart" component={Working} />
          <Route path="/settings" component={Working} />
        </div>
      </Router>
    </Fragment>
  ) : null;
};

const mapStateToProps = ({ etsyGuestCartData: { etsyGuestCartData } }) => ({
  etsyGuestCartData,
});

const mapDispatchToProps = {
  receivedEtsyGuestId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
