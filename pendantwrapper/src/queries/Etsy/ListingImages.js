import gql from 'graphql-tag';

export default gql`
  query GetListingImages($listing_id: String) {
    listingimages(listing_id: $listing_id) {
      listing_id
      listing_image_id
      rank
      url_75x75
      url_570xN
      url_170x135
      url_fullxfull
      full_width
      full_height
    }
  }
`;
