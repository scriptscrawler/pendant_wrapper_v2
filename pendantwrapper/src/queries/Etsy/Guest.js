import gql from 'graphql-tag';

export default gql`
  {
    guest {
      guest_id
      checkout_url
    }
  }
`;
