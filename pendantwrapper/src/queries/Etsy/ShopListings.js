import gql from 'graphql-tag';

export default gql`
  {
    shoplistings {
      listing_id
      state
      user_id
      category_id
      title
      description
      price
      currency_code
      quantity
      tags
      category_path
      materials
      url
      shop_section_id
      views
      shipping_template_id
      processing_min
      processing_max
    }
  }
`;
