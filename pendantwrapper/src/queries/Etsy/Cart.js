import gql from 'graphql-tag';

export default gql`
  query CartQuery($guest_id: String) {
    cart(guest_id: $guest_id) {
      cart_id
      shop_name
      message_to_seller
      destination_country_id
      coupon_code
      currency_code
      total
      subtotal
      shipping_cost
      tax_cost
      discount_amount
      shipping_discount_amount
      tax_discount_amount
      url
      listings {
        listing_id
        state
        user_id
        category_id
        title
        description
        price
        currency_code
        quantity
        shop_section_id
        url
        views
        shipping_template_id
        processing_min
        processing_max
      }
    }
  }
`;
