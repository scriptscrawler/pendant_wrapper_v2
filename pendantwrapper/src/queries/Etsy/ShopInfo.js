import gql from 'graphql-tag';

export default gql`
  {
    shopinfo {
      shop_id
      shop_name
      policy_welcome
      policy_payment
      policy_shipping
      policy_refunds
      announcement
    }
  }
`;
