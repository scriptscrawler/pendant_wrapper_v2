const express = require('express');
const htmlRouter = express.Router();

htmlRouter.get('/', (req, { send }) => {
  send('public/dist/index.html');
});

module.exports = htmlRouter;
