const htmlRoutes = require('./htmlRoutes.js');
const loggingRoutes = require('./logging.js');
const mailgunRoutes = require('./mailgun.js');
const etsyRouter = require('./etsyRoutes');

module.exports = function(app) {
  app.use(htmlRoutes);

  app.use('/log', loggingRoutes);

  app.use('/mail', mailgunRoutes);

  app.use('/etsy', etsyRouter);

  app.get('*', (req, res) => {
    res.redirect('/');
  });
};
