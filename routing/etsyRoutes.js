const axios = require('axios');
const express = require('express');
const etsyRouter = express.Router();
const { ETSY_ACCESS_KEY: eak } = process.env;

const BASE_URL = 'https://openapi.etsy.com/v2/';
const GUESTS = 'guests/';

etsyRouter.get('/guestid', (req, res) => {
  axios
    .get(`${BASE_URL}${GUESTS}generator?api_key=${eak}`)
    .then(({ data: { results } }) => {
      res.json(results[0]);
    })
    .catch(err => {
      return console.log(err);
    });
});

module.exports = etsyRouter;
