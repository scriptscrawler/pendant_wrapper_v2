const cors = require('cors');
const { MAIL_GUN_KEY: apiKey, MAIL_GUN_DOMAIN: domain } = process.env;
const mailgun = require('mailgun-js')({
  apiKey,
  domain,
});
const express = require('express');
const mailGunRouter = express.Router();

mailGunRouter.use(cors()).post('/pendant/mail', (req, res) => {
  const { email: from, subject, body: text } = req.body;
  const to = 'candyjkramer@gmail.com';

  const message = {
    from,
    to,
    subject,
    text,
  };
  mailgun.messages().send(message, function(error, body) {
    if (error) {
      console.log(error);
    }

    res.send(body);
  });
});

module.exports = mailGunRouter;
